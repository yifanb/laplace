#include <iostream>
#include <sstream>
#include <math.h>
#include <cuda.h>
#include <cuda_runtime.h>
#include <omp.h>

#define CHECK_CUDA(func)                                                \
{                                                                       \
    cudaError_t status = (func);                                        \
    if (status != cudaSuccess) {                                        \
        printf("CUDA API failed at line %d with error: %s (%d)\n",      \
               __LINE__, cudaGetErrorString(status), status);           \
        return EXIT_FAILURE;                                            \
    }                                                                   \
}

#define idx2d(i,j,n) ( (i) + ((j)*(n)) )
#define idx3d(i,j,k,nx,ny) ( (i) + ((j)*(nx)) + ((k)*(nx)*(ny)) )

#define JACOBI 0
#define GS 1

#define MAX_NUM_DEVICES 2

using namespace std;

__device__
void _sum(int n, double *A, double *s, int p)
{
	__shared__ double cache[2048];
	int index = threadIdx.x;
	int ind2 = index*2;
	int size = blockDim.x*2;
    int first = blockIdx.x*size;
	int step = gridDim.x*size;
	int ni, stride, fst, snd;
	double ss(0.0);
	double a, c;
	for (int k = first; k < n; k += step) {
		if (k + ind2 < n) {
			a = 1.0;
			c = A[k + ind2];
			for (int i = 0; i < p; ++i) {
				a *= c;
			}
			cache[ind2] = a;
		}
		if (k + ind2 + 1 < n) {
			a = 1.0;
			c = A[k + ind2 + 1];
			for (int i = 0; i < p; ++i) {
				a *= c;
			}
			cache[ind2 + 1] = a;
		}
		__syncthreads();
		ni = n - k;
		ni = (ni < size) ? ni : size;
		int adj = (ni % 2 == 1) ? 1:0;
		ni >>= 1;
		stride = 1;
		while (ni > 0) {
			//if (index == 0) {
				//printf("%d ", ni);
			//}
			if (index < ni) {
				fst = ind2*stride;
				snd = fst + stride;
				cache[fst] += cache[snd];
				//if (index == 0) {
					//printf("%f %f %f \n", cache[0], cache[1], cache[2]);
				//}
			}
			__syncthreads();
			stride <<= 1;
			if (adj == 1)
				ni += 1;
			adj = (ni % 2 == 1) ? 1:0;
			ni >>= 1;
		}
		if (index == 0) {
			ss += cache[0];
		}
	}
	if (index == 0) {
		s[blockIdx.x] = ss;
	}
}

__global__ 
void sum(int n, double *A, double *s)
{
	_sum(n, A, s, 1);
}

__global__ 
void square_sum(int n, double *A, double *s)
{
	_sum(n, A, s, 2);
}

__global__
void copy(int n, double *A, double *A0)
{
    int index = blockIdx.x * blockDim.x + threadIdx.x;
	if (index < n) {
		A0[index] = A[index];
	}
}

template<int MaxXBlkDim, int MaxYBlkDim>
__global__ void rbgs_smooth(int nx, int ny, int nz, int flag, double *A, double* f, double dx, double omega)
{
	__shared__ double cache[MaxXBlkDim*MaxYBlkDim*2];
    int i = blockIdx.x * (MaxXBlkDim - 2) + threadIdx.x;
    int j = blockIdx.y * (MaxYBlkDim - 2) + threadIdx.y;
	int ii = threadIdx.x;
	int jj = threadIdx.y;
	double next;
	if (i < nx && j < ny) {
		cache[idx3d(ii, jj, 0, MaxXBlkDim, MaxYBlkDim)] = A[idx3d(i, j, 0, nx, ny)];
		cache[idx3d(ii, jj, 1, MaxXBlkDim, MaxYBlkDim)] = A[idx3d(i, j, 1, nx, ny)];
	}
	for (int k = 1; k < nz - 1; ++k) {
		int index_g = idx3d(i, j, k, nx, ny);
		int index = idx3d(ii, jj, 1, MaxXBlkDim, MaxYBlkDim);
		if (i < nx && j < ny) {
			next = A[idx3d(i, j, k + 1, nx, ny)];
		}
		__syncthreads();
		if (ii > 0 && ii < MaxXBlkDim - 1 && jj > 0 && jj < MaxYBlkDim - 1 && i < nx - 1 && j < ny - 1) {
			if ((flag == 0 && (i + j + k) % 2 == 0) || (flag == 1 && (i + j + k) % 2 == 1)) {
				double a[6];
				a[0] = cache[idx3d(ii - 1, jj, 1, MaxXBlkDim, MaxYBlkDim)];
				a[1] = cache[idx3d(ii + 1, jj, 1, MaxXBlkDim, MaxYBlkDim)];
				a[2] = cache[idx3d(ii, jj - 1, 1, MaxXBlkDim, MaxYBlkDim)];
				a[3] = cache[idx3d(ii, jj + 1, 1, MaxXBlkDim, MaxYBlkDim)];
				a[4] = cache[idx3d(ii, jj, 0, MaxXBlkDim, MaxYBlkDim)];
				a[5] = next;
				A[index_g] = (1.0 - omega)*cache[index] + \
							 omega*((-dx*dx*f[index_g] + a[0] + a[1] + a[2] + a[3] + a[4] + a[5])/6.0);
			}
		}
		__syncthreads();
		if (i < nx && j < ny) {
			cache[idx3d(ii, jj, 0, MaxXBlkDim, MaxYBlkDim)] = cache[index];
			cache[index] = next;
		}
	}
}

template<int MaxXBlkDim, int MaxYBlkDim>
__global__ void residual(int nx, int ny, int nz, double *res, double *A, double* f, double dx)
{
	__shared__ double cache[MaxXBlkDim*MaxYBlkDim*2];
    int i = blockIdx.x * (MaxXBlkDim - 2) + threadIdx.x;
    int j = blockIdx.y * (MaxYBlkDim - 2) + threadIdx.y;
	int ii = threadIdx.x;
	int jj = threadIdx.y;
	double next;
	if (i < nx && j < ny) {
		cache[idx3d(ii, jj, 0, MaxXBlkDim, MaxYBlkDim)] = A[idx3d(i, j, 0, nx, ny)];
		cache[idx3d(ii, jj, 1, MaxXBlkDim, MaxYBlkDim)] = A[idx3d(i, j, 1, nx, ny)];
	}
	for (int k = 1; k < nz - 1; ++k) {
		int index_g = idx3d(i, j, k, nx, ny);
		int index = idx3d(ii, jj, 1, MaxXBlkDim, MaxYBlkDim);
		if (i < nx && j < ny) {
			next = A[idx3d(i, j, k + 1, nx, ny)];
		}
		__syncthreads();
		if (ii > 0 && ii < MaxXBlkDim - 1 && jj > 0 && jj < MaxYBlkDim - 1 && i < nx - 1 && j < ny - 1) {
			double a[6];
			a[0] = cache[idx3d(ii - 1, jj, 1, MaxXBlkDim, MaxYBlkDim)];
			a[1] = cache[idx3d(ii + 1, jj, 1, MaxXBlkDim, MaxYBlkDim)];
			a[2] = cache[idx3d(ii, jj - 1, 1, MaxXBlkDim, MaxYBlkDim)];
			a[3] = cache[idx3d(ii, jj + 1, 1, MaxXBlkDim, MaxYBlkDim)];
			a[4] = cache[idx3d(ii, jj, 0, MaxXBlkDim, MaxYBlkDim)];
			a[5] = next;
			res[index_g] = 1.0/dx/dx*(6.0*cache[index] - \
						  (a[0] + a[1] + a[2] + a[3] + a[4] + a[5])) + f[index_g];
		}
		__syncthreads();
		if (i < nx && j < ny) {
			cache[idx3d(ii, jj, 0, MaxXBlkDim, MaxYBlkDim)] = cache[index];
			cache[index] = next;
		}
	}
}

template<int MaxXBlkDim, int MaxYBlkDim>
__global__ void restrict_(int nx, int ny, int nz, double *res0, double* f1)
{
	__shared__ double cache[MaxXBlkDim*MaxYBlkDim*3];
	int i = blockIdx.x * (MaxXBlkDim - 1) + threadIdx.x + 1;
	int j = blockIdx.y * (MaxYBlkDim - 1) + threadIdx.y + 1;
	int ii = threadIdx.x;
	int jj = threadIdx.y;
	double next;
	if (i < nx && j < ny) {
		cache[idx2d(ii, jj, MaxXBlkDim)] = res0[idx3d(i, j, 1, nx, ny)];
	}
	for (int k = 2; k < nz - 2; k += 2) {
		if (i < nx - 1 && j < ny - 1) {
			next = res0[idx3d(i, j, k + 1, nx, ny)];
			cache[idx3d(ii, jj, 1, MaxXBlkDim, MaxYBlkDim)] = res0[idx3d(i, j, k, nx, ny)];
			cache[idx3d(ii, jj, 2, MaxXBlkDim, MaxYBlkDim)] = next;
		}
		__syncthreads();
		if (i % 2 == 0 && j % 2 == 0 && i < nx - 1 && j < nx - 1) {
			f1[idx3d(i/2, j/2, k/2, nx/2 + 1, ny/2 + 1)] = \
														   cache[idx3d(ii, jj, 1, MaxXBlkDim, MaxYBlkDim)]/8.0 + (
																   cache[idx3d(ii - 1, jj, 1, MaxXBlkDim, MaxYBlkDim)] + 
																   cache[idx3d(ii + 1, jj, 1, MaxXBlkDim, MaxYBlkDim)] +
																   cache[idx3d(ii, jj - 1, 1, MaxXBlkDim, MaxYBlkDim)] +
																   cache[idx3d(ii, jj + 1, 1, MaxXBlkDim, MaxYBlkDim)] +
																   cache[idx3d(ii, jj, 0, MaxXBlkDim, MaxYBlkDim)] + 
																   cache[idx3d(ii, jj, 2, MaxXBlkDim, MaxYBlkDim)])/16.0 + (
																   cache[idx3d(ii - 1, jj - 1, 1, MaxXBlkDim, MaxYBlkDim)] + 
																   cache[idx3d(ii - 1, jj + 1, 1, MaxXBlkDim, MaxYBlkDim)] + 
																   cache[idx3d(ii + 1, jj - 1, 1, MaxXBlkDim, MaxYBlkDim)] +
																   cache[idx3d(ii + 1, jj + 1, 1, MaxXBlkDim, MaxYBlkDim)] +
																   cache[idx3d(ii, jj - 1, 0, MaxXBlkDim, MaxYBlkDim)] +
																   cache[idx3d(ii, jj + 1, 0, MaxXBlkDim, MaxYBlkDim)] +
																   cache[idx3d(ii, jj - 1, 2, MaxXBlkDim, MaxYBlkDim)] +
																   cache[idx3d(ii, jj + 1, 2, MaxXBlkDim, MaxYBlkDim)] +
																   cache[idx3d(ii - 1, jj, 0, MaxXBlkDim, MaxYBlkDim)] + 
																   cache[idx3d(ii + 1, jj, 0, MaxXBlkDim, MaxYBlkDim)] + 
																   cache[idx3d(ii - 1, jj, 2, MaxXBlkDim, MaxYBlkDim)] + 
																   cache[idx3d(ii + 1, jj, 2, MaxXBlkDim, MaxYBlkDim)])/32.0 + (
																   cache[idx3d(ii - 1, jj - 1, 0, MaxXBlkDim, MaxYBlkDim)] + 
																   cache[idx3d(ii - 1, jj + 1, 0, MaxXBlkDim, MaxYBlkDim)] + 
																   cache[idx3d(ii + 1, jj - 1, 0, MaxXBlkDim, MaxYBlkDim)] +
																   cache[idx3d(ii + 1, jj + 1, 0, MaxXBlkDim, MaxYBlkDim)] +
																   cache[idx3d(ii - 1, jj - 1, 2, MaxXBlkDim, MaxYBlkDim)] + 
																   cache[idx3d(ii - 1, jj + 1, 2, MaxXBlkDim, MaxYBlkDim)] + 
																   cache[idx3d(ii + 1, jj - 1, 2, MaxXBlkDim, MaxYBlkDim)] +
																   cache[idx3d(ii + 1, jj + 1, 2, MaxXBlkDim, MaxYBlkDim)])/64.0;
			//printf("%d %d %d %d %f\n", i/2, j/2, k/2, nx/2 + 1, f1[idx3d(i/2, j/2, k/2, nx/2 + 1, ny/2 + 1)]);
		}
		__syncthreads();
		if (i < nx && j < ny) {
			cache[idx2d(ii, jj, MaxXBlkDim)] = next;
		}
	}
}

template<int MaxXBlkDim, int MaxYBlkDim>
__global__ void prolong(int nx, int ny, int nz, double *A1, double* A0)
{
	__shared__ double cache[MaxXBlkDim*MaxYBlkDim*2];
	int i = blockIdx.x * (MaxXBlkDim - 1) + threadIdx.x;
	int j = blockIdx.y * (MaxYBlkDim - 1) + threadIdx.y;
	int ii = threadIdx.x;
	int jj = threadIdx.y;
	//if (ii==0 && jj==0){
	//printf("%f", A1[idx3d(2,2,2,nx,ny)]);
	//}
	double next;
	if (i < nx && j < ny) {
		cache[idx2d(ii, jj, MaxXBlkDim)] = A1[idx3d(i, j, 0, nx, ny)];
	}
	for (int k = 1; k < nz; ++k) {
		if (i < nx && j < ny) {
			next = A1[idx3d(i, j, k, nx, ny)];
			cache[idx3d(ii, jj, 1, MaxXBlkDim, MaxYBlkDim)] = next;
		}
		__syncthreads();
		if (i < nx && j < ny && ii > 0 && jj > 0) {
			//printf("%d %d %d %f\n", i, j, 2*nx - 1, next);
			A0[idx3d(2*i, 2*j, 2*k, 2*nx - 1, 2*ny - 1)] += next;

			A0[idx3d(2*i - 1, 2*j, 2*k, 2*nx - 1, 2*ny - 1)] += (next + cache[idx3d(ii - 1, jj, 1, MaxXBlkDim, MaxYBlkDim)])/2.0;
			A0[idx3d(2*i, 2*j - 1, 2*k, 2*nx - 1, 2*ny - 1)] += (next + cache[idx3d(ii, jj - 1, 1, MaxXBlkDim, MaxYBlkDim)])/2.0;
			A0[idx3d(2*i, 2*j, 2*k - 1, 2*nx - 1, 2*ny - 1)] += (next + cache[idx3d(ii, jj, 0, MaxXBlkDim, MaxYBlkDim)])/2.0;

			A0[idx3d(2*i - 1, 2*j - 1, 2*k, 2*nx - 1, 2*ny - 1)] += (next +
					cache[idx3d(ii - 1, jj, 1, MaxXBlkDim, MaxYBlkDim)] + 
					cache[idx3d(ii, jj - 1, 1, MaxXBlkDim, MaxYBlkDim)] + 
					cache[idx3d(ii - 1, jj - 1, 1, MaxXBlkDim, MaxYBlkDim)])/4.0;
			A0[idx3d(2*i - 1, 2*j, 2*k - 1, 2*nx - 1, 2*ny - 1)] += (next +
					cache[idx3d(ii - 1, jj, 1, MaxXBlkDim, MaxYBlkDim)] + 
					cache[idx3d(ii, jj, 0, MaxXBlkDim, MaxYBlkDim)] + 
					cache[idx3d(ii - 1, jj, 0, MaxXBlkDim, MaxYBlkDim)])/4.0;
			A0[idx3d(2*i, 2*j - 1, 2*k - 1, 2*nx - 1, 2*ny - 1)] += (next +
					cache[idx3d(ii, jj - 1, 1, MaxXBlkDim, MaxYBlkDim)] + 
					cache[idx3d(ii, jj, 0, MaxXBlkDim, MaxYBlkDim)] + 
					cache[idx3d(ii, jj - 1, 0, MaxXBlkDim, MaxYBlkDim)])/4.0;

			A0[idx3d(2*i - 1, 2*j - 1, 2*k - 1, 2*nx - 1, 2*ny - 1)] += (next +
					cache[idx3d(ii - 1, jj, 1, MaxXBlkDim, MaxYBlkDim)] +
					cache[idx3d(ii, jj - 1, 1, MaxXBlkDim, MaxYBlkDim)] +
					cache[idx3d(ii, jj, 0, MaxXBlkDim, MaxYBlkDim)] +
					cache[idx3d(ii - 1, jj - 1, 1, MaxXBlkDim, MaxYBlkDim)] + 
					cache[idx3d(ii - 1, jj, 0, MaxXBlkDim, MaxYBlkDim)] + 
					cache[idx3d(ii, jj - 1, 0, MaxXBlkDim, MaxYBlkDim)] + 
					cache[idx3d(ii - 1, jj - 1, 0, MaxXBlkDim, MaxYBlkDim)])/8.0;
		}
		__syncthreads();
		if (i < nx && j < ny) {
			cache[idx2d(ii, jj, MaxXBlkDim)] = next;
		}
	}
}

int rbgs(int ndev, int nlevel, int* nn, int* N, int niter, double dx, double ***A_dev, double*** f_dev,
		double*** res_dev, double** ss_dev, double** s_dev, float& gpu_time)
{
	//int N = pow(nn[0], 3);
	//double omega = 2.0/3.0;
	double omega = 1.0;
	int npre = 2;
	int npost = 2;
	int ncoarse = 1;
	double wunit(0.0);
	for (int i = 0; i < nlevel - 1; ++i) {
		wunit += (npre + npost) * \
				 double(nn[idx3d(0, i, 0, ndev, nlevel)])/nn[idx3d(0, 0, 0, ndev, nlevel)] * \
				 double(nn[idx3d(0, i, 1, ndev, nlevel)])/nn[idx3d(0, 0, 1, ndev, nlevel)] * \
				 double(nn[idx3d(0, i, 2, ndev, nlevel)])/nn[idx3d(0, 0, 2, ndev, nlevel)];
	}
	wunit += ncoarse* \
			 double(nn[idx3d(0, nlevel - 1, 0, ndev, nlevel)])/nn[idx3d(0, 0, 0, ndev, nlevel)] * \
			 double(nn[idx3d(0, nlevel - 1, 1, ndev, nlevel)])/nn[idx3d(0, 0, 1, ndev, nlevel)] * \
			 double(nn[idx3d(0, nlevel - 1, 2, ndev, nlevel)])/nn[idx3d(0, 0, 2, ndev, nlevel)];
	int ncycle = ceil(niter/wunit);
	//int ncycle = floor(niter/wunit);
	//cout << ncycle << endl;
	cout << "Total work unit: " << ncycle*wunit << endl;

	int  nx = 32;
	int  ny = 32;
	dim3 blockSize = dim3(nx, ny);
	dim3 numBlocks = dim3((nn[idx3d(0, 0, 0, ndev, nlevel)] + nx - 5) / (nx - 2),
			(nn[idx3d(0, 0, 1, ndev, nlevel)] + ny - 5) / (ny - 2));

	nx = 31;
	ny = 31;
	dim3 blockSize1 = dim3(nx, ny);
	dim3 numBlocks1 = dim3((nn[idx3d(0, 0, 0, ndev, nlevel)] + nx - 4) / (nx - 1),
			(nn[idx3d(0, 0, 1, ndev, nlevel)] + ny - 4) / (ny - 1));
	//dim3 numBlocks1 = dim3(1, 1);

	// 2 GPUs
	bool p2p = false;
	if (p2p && ndev == 2) {
		int canAccessPeer = 0;
		CHECK_CUDA(cudaDeviceCanAccessPeer(&canAccessPeer, 0, 1));
		if (canAccessPeer) {
			cudaSetDevice(0);
			CHECK_CUDA(cudaDeviceEnablePeerAccess(1, 0));
		}
		canAccessPeer = 0;
		CHECK_CUDA(cudaDeviceCanAccessPeer(&canAccessPeer, 1, 0));
		if (canAccessPeer) {
			cudaSetDevice(1);
			CHECK_CUDA(cudaDeviceEnablePeerAccess(0, 0));
		}
	}

	cudaStream_t compute_stream[MAX_NUM_DEVICES];
	cudaEvent_t compute_done[MAX_NUM_DEVICES];
	for (int id = 0; id < ndev; ++id) {
		cudaSetDevice(id);
		CHECK_CUDA(cudaStreamCreate(compute_stream + id));
		CHECK_CUDA(cudaEventCreateWithFlags(compute_done + id, cudaEventDisableTiming));
	}

	cudaStream_t top, bottom;
	cudaEvent_t memcpy_top_done, memcpy_bottom_done;
	CHECK_CUDA(cudaStreamCreate(&top));
	CHECK_CUDA(cudaStreamCreate(&bottom));
	CHECK_CUDA(cudaEventCreateWithFlags(&memcpy_top_done, cudaEventDisableTiming));
	CHECK_CUDA(cudaEventCreateWithFlags(&memcpy_bottom_done, cudaEventDisableTiming));

	//cudaEvent_t start, stop;
	//cudaEventCreate(&start);
	//cudaEventCreate(&stop);
	//cudaEventRecord(start);

	int n = nn[0];
	double start = omp_get_wtime();
#pragma omp parallel num_threads(ndev)
	{
		int id = omp_get_thread_num();
		cudaSetDevice(id);
		//cout << id << endl;
		//cout << nn[idx3d(id, 0, 0, ndev, nlevel)] << " " << nn[idx3d(id, 0, 1, ndev, nlevel)] << " " << nn[idx3d(id, 0, 2, ndev, nlevel)] << endl;


		for (int k = 0; k < ncycle; ++k) {
			// First half
			if (nlevel > 1) {
				for (int j = 0; j < npre; ++j) {
					rbgs_smooth<32, 32><<<numBlocks, blockSize, 0, compute_stream[id]>>>(
							nn[idx3d(id, 0, 0, ndev, nlevel)],
							nn[idx3d(id, 0, 1, ndev, nlevel)],
							nn[idx3d(id, 0, 2, ndev, nlevel)],
							0, A_dev[id][0], f_dev[id][0],
							1.0/(nn[idx3d(id, 0, 0, ndev, nlevel)] - 1.0), omega);
					rbgs_smooth<32, 32><<<numBlocks, blockSize, 0, compute_stream[id]>>>(
							nn[idx3d(id, 0, 0, ndev, nlevel)],
							nn[idx3d(id, 0, 1, ndev, nlevel)],
							nn[idx3d(id, 0, 2, ndev, nlevel)],
							1, A_dev[id][0], f_dev[id][0],
							1.0/(nn[idx3d(id, 0, 0, ndev, nlevel)] - 1.0), omega);
					cudaEventRecord(compute_done[id], compute_stream[id]);
#pragma omp barrier 

#pragma omp single nowait
					{
						//0 -> 1
						for (int id = 0; id < ndev; ++id) {
							cudaStreamWaitEvent(top, compute_done[id], 0);
						}
						cudaMemcpyAsync(
								A_dev[1][0],
								A_dev[0][0] + (nn[idx3d(0, 0, 2, ndev, nlevel)] - 5)*n*n,
								2*n*n*sizeof(double), cudaMemcpyDeviceToDevice, top);
						cudaEventRecord(memcpy_top_done, top);
						//1 -> 0
						for (int id = 0; id < ndev; ++id) {
							cudaStreamWaitEvent(bottom, compute_done[id], 0);
						}
						cudaMemcpyAsync(
								A_dev[0][0] + (nn[idx3d(0, 0, 2, ndev, nlevel)] - 2)*n*n,
								A_dev[1][0] + 3*n*n,
								2*n*n*sizeof(double), cudaMemcpyDeviceToDevice, bottom);
						cudaEventRecord(memcpy_bottom_done, bottom);
					}
				}

				cudaStreamWaitEvent(compute_stream[id], memcpy_top_done, 0);
				cudaStreamWaitEvent(compute_stream[id], memcpy_bottom_done, 0);

				residual<32, 32><<<numBlocks, blockSize, 0, compute_stream[id]>>>(
						nn[idx3d(id, 0, 0, ndev, nlevel)],
						nn[idx3d(id, 0, 1, ndev, nlevel)],
						nn[idx3d(id, 0, 2, ndev, nlevel)],
						res_dev[id][0], A_dev[id][0], f_dev[id][0],
						1.0/(nn[idx3d(id, 0, 0, ndev, nlevel)] - 1.0));
				restrict_<31, 31><<<numBlocks1, blockSize1, 0, compute_stream[id]>>>(
						nn[idx3d(id, 0, 0, ndev, nlevel)],
						nn[idx3d(id, 0, 1, ndev, nlevel)],
						nn[idx3d(id, 0, 2, ndev, nlevel)],
						res_dev[id][0], f_dev[id][1]);
			}

			for (int i = 1; i < nlevel - 1; ++i) {
				for (int j = 0; j < npre; ++j) {
					rbgs_smooth<32, 32><<<numBlocks, blockSize, 0, compute_stream[id]>>>(
							nn[idx3d(id, i, 0, ndev, nlevel)],
							nn[idx3d(id, i, 1, ndev, nlevel)],
							nn[idx3d(id, i, 2, ndev, nlevel)],
							0, A_dev[id][i], f_dev[id][i],
							1.0/(nn[idx3d(id, i, 0, ndev, nlevel)] - 1.0), omega);
					rbgs_smooth<32, 32><<<numBlocks, blockSize, 0, compute_stream[id]>>>(
							nn[idx3d(id, i, 0, ndev, nlevel)],
							nn[idx3d(id, i, 1, ndev, nlevel)],
							nn[idx3d(id, i, 2, ndev, nlevel)],
							1, A_dev[id][i], f_dev[id][i],
							1.0/(nn[idx3d(id, i, 0, ndev, nlevel)] - 1.0), omega);
				}
				// restrict
				residual<32, 32><<<numBlocks, blockSize, 0, compute_stream[id]>>>(
						nn[idx3d(id, i, 0, ndev, nlevel)],
						nn[idx3d(id, i, 1, ndev, nlevel)],
						nn[idx3d(id, i, 2, ndev, nlevel)],
						res_dev[id][i], A_dev[id][i], f_dev[id][i],
						1.0/(nn[idx3d(id, i, 0, ndev, nlevel)] - 1.0));
				restrict_<31, 31><<<numBlocks1, blockSize1, 0, compute_stream[id]>>>(
						nn[idx3d(id, i, 0, ndev, nlevel)],
						nn[idx3d(id, i, 1, ndev, nlevel)],
						nn[idx3d(id, i, 2, ndev, nlevel)],
						res_dev[id][i], f_dev[id][i + 1]);
			}

			// Coursest grid
			for (int j = 0; j < ncoarse; ++j) {
				rbgs_smooth<32, 32><<<numBlocks, blockSize, 0, compute_stream[id]>>>(
						nn[idx3d(id, nlevel - 1, 0, ndev, nlevel)],
						nn[idx3d(id, nlevel - 1, 1, ndev, nlevel)],
						nn[idx3d(id, nlevel - 1, 2, ndev, nlevel)],
						0, A_dev[id][nlevel - 1], f_dev[id][nlevel - 1],
						1.0/(nn[idx3d(id, nlevel - 1, 0, ndev, nlevel)] - 1.0), omega);
				rbgs_smooth<32, 32><<<numBlocks, blockSize, 0, compute_stream[id]>>>(
						nn[idx3d(id, nlevel - 1, 0, ndev, nlevel)],
						nn[idx3d(id, nlevel - 1, 1, ndev, nlevel)],
						nn[idx3d(id, nlevel - 1, 2, ndev, nlevel)],
						1, A_dev[id][nlevel - 1], f_dev[id][nlevel - 1],
						1.0/(nn[idx3d(id, nlevel - 1, 0, ndev, nlevel)] - 1.0), omega);
			}
#pragma omp barrier 

#pragma omp single
			{
				//0 -> 1
				for (int id = 0; id < ndev; ++id) {
					cudaStreamWaitEvent(top, compute_done[id], 0);
				}
				cudaMemcpy(
						A_dev[1][0],
						A_dev[0][0] + (nn[idx3d(0, 0, 2, ndev, nlevel)] - 5)*n*n,
						2*n*n*sizeof(double), cudaMemcpyDeviceToDevice);
				cudaEventRecord(memcpy_top_done, top);
				//1 -> 0
				for (int id = 0; id < ndev; ++id) {
					cudaStreamWaitEvent(bottom, compute_done[id], 0);
				}
				cudaMemcpy(
						A_dev[0][0] + (nn[idx3d(0, 0, 2, ndev, nlevel)] - 2)*n*n,
						A_dev[1][0] + 3*n*n,
						2*n*n*sizeof(double), cudaMemcpyDeviceToDevice);
				cudaEventRecord(memcpy_top_done, bottom);
			}

			// Second half
			for (int i = nlevel - 1; i > 1; --i) {
				prolong<31, 31><<<numBlocks1, blockSize1, 0, compute_stream[id]>>>(
						nn[idx3d(id, i, 0, ndev, nlevel)],
						nn[idx3d(id, i, 1, ndev, nlevel)],
						nn[idx3d(id, i, 2, ndev, nlevel)],
						A_dev[id][i], A_dev[id][i - 1]);
				for (int j = 0; j < npost; ++j) {
					rbgs_smooth<32, 32><<<numBlocks, blockSize, 0, compute_stream[id]>>>(
							nn[idx3d(0, i - 1, 0, ndev, nlevel)],
							nn[idx3d(0, i - 1, 1, ndev, nlevel)],
							nn[idx3d(0, i - 1, 2, ndev, nlevel)],
							0, A_dev[id][i - 1], f_dev[id][i - 1],
							1.0/(nn[idx3d(id, i - 1, 0, ndev, nlevel)] - 1.0), omega);
					rbgs_smooth<32, 32><<<numBlocks, blockSize, 0, compute_stream[id]>>>(
							nn[idx3d(0, i - 1, 0, ndev, nlevel)],
							nn[idx3d(0, i - 1, 1, ndev, nlevel)],
							nn[idx3d(0, i - 1, 2, ndev, nlevel)],
							1, A_dev[id][i - 1], f_dev[id][i - 1],
							1.0/(nn[idx3d(id, i - 1, 0, ndev, nlevel)] - 1.0), omega);
				}
			}
			if (nlevel > 1) {
				prolong<31, 31><<<numBlocks1, blockSize1, 0, compute_stream[id]>>>(
						nn[idx3d(id, 1, 0, ndev, nlevel)],
						nn[idx3d(id, 1, 1, ndev, nlevel)],
						nn[idx3d(id, 1, 2, ndev, nlevel)],
						A_dev[id][1], A_dev[id][0]);
				for (int j = 0; j < npost; ++j) {
					rbgs_smooth<32, 32><<<numBlocks, blockSize, 0, compute_stream[id]>>>(
							nn[idx3d(id, 0, 0, ndev, nlevel)],
							nn[idx3d(id, 0, 1, ndev, nlevel)],
							nn[idx3d(id, 0, 2, ndev, nlevel)],
							0, A_dev[id][0], f_dev[id][0],
							1.0/(nn[idx3d(id, 0, 0, ndev, nlevel)] - 1.0), omega);
					rbgs_smooth<32, 32><<<numBlocks, blockSize, 0, compute_stream[id]>>>(
							nn[idx3d(id, 0, 0, ndev, nlevel)],
							nn[idx3d(id, 0, 1, ndev, nlevel)],
							nn[idx3d(id, 0, 2, ndev, nlevel)],
							1, A_dev[id][0], f_dev[id][0],
							1.0/(nn[idx3d(id, 0, 0, ndev, nlevel)] - 1.0), omega);
					cudaEventRecord(compute_done[id], compute_stream[id]);
#pragma omp barrier 

#pragma omp single nowait
					{
						//0 -> 1
						for (int id = 0; id < ndev; ++id) {
							cudaStreamWaitEvent(top, compute_done[id], 0);
						}
						cudaMemcpyAsync(
								A_dev[1][0],
								A_dev[0][0] + (nn[idx3d(0, 0, 2, ndev, nlevel)] - 5)*n*n,
								2*n*n*sizeof(double), cudaMemcpyDeviceToDevice, top);
						cudaEventRecord(memcpy_top_done, top);
						//1 -> 0
						for (int id = 0; id < ndev; ++id) {
							cudaStreamWaitEvent(bottom, compute_done[id], 0);
						}
						cudaMemcpyAsync(
								A_dev[0][0] + (nn[idx3d(0, 0, 2, ndev, nlevel)] - 2)*n*n,
								A_dev[1][0] + 3*n*n,
								2*n*n*sizeof(double), cudaMemcpyDeviceToDevice, bottom);
						cudaEventRecord(memcpy_bottom_done, bottom);
					}
					cudaStreamWaitEvent(compute_stream[id], memcpy_top_done, 0);
					cudaStreamWaitEvent(compute_stream[id], memcpy_bottom_done, 0);
				}
			}
		}
		//cout << nn[idx3d(id, 0, 0, ndev, nlevel)] << endl;

		//int nblocks = 1;
		//square_sum<<<nblocks, 1024>>>(N[idx2d(id, 0, ndev)] - 5*n*n*id, A_dev[id][0] + 5*n*n*id, ss_dev[id]);
		//sum<<<1, nblocks>>>(nblocks, ss_dev[id], s_dev[id]);
	}

	//cudaSetDevice(0);
	//int nblocks = 64;
	//square_sum<<<nblocks, 1024>>>(N[idx2d(0, 0, ndev)] - 3*n*n, A_dev[0][0], ss_dev[0]);
	//sum<<<1, nblocks>>>(nblocks, ss_dev[0], s_dev[0]);

	//cudaSetDevice(1);
	//square_sum<<<nblocks, 1024>>>(N[idx2d(1, 0, ndev)] - 2*n*n, A_dev[1][0] + 2*n*n, ss_dev[1]);
	//sum<<<1, nblocks>>>(nblocks, ss_dev[1], s_dev[1]);
	//cudaDeviceSynchronize();
	//cudaError_t cudaStatus = cudaGetLastError();
	//if (cudaStatus != cudaSuccess) {
		//cout<<"Update kernel failed: "<<cudaGetErrorString(cudaStatus)<<endl;
		//return EXIT_FAILURE;
	//}

	cudaDeviceSynchronize();

	double stop = omp_get_wtime();
	gpu_time = (stop - start)*1000;
	//cudaEventRecord(stop);
	//cudaEventSynchronize(stop);
	//cudaEventElapsedTime(&gpu_time, start, stop);
	//cudaEventDestroy(start);
	//cudaEventDestroy(stop);

	return EXIT_SUCCESS;
}

int main(int argc, char* argv[])
{
    if (argc < 3) {
        cerr << "Usage: ./jacobi <N> <# of iterations>." << std::endl;
        return 1;
    }
    istringstream ss(string(argv[1]) + " " + string(argv[2]) + " " + string(argv[3]));
	int n, niter, nlevel;
	ss >> n >> niter >> nlevel;
	double dx = 1.0/n;
	//n += 1;

	// v cycle
	//int nlevel = 4;
	int num_devices = 0;
    CHECK_CUDA(cudaGetDeviceCount(&num_devices));

	int *nn, *N;
	nn = new int[num_devices*nlevel*3];
	N = new int[num_devices*nlevel];
	int nl = n/num_devices;
	for (int id = 0; id < num_devices; ++id) {
		nn[idx3d(id, 0, 0, num_devices, nlevel)] = n + 1; // 2 layers ghost
		nn[idx3d(id, 0, 1, num_devices, nlevel)] = n + 1;
		nn[idx3d(id, 0, 2, num_devices, nlevel)] = nl + 3;
		N[idx2d(id, 0, num_devices)] = (nl + 3)*(n + 1)*(n + 1);
		for (int i = 1; i < nlevel; i++) {
			N[idx2d(id, i, num_devices)] = 1;
			for (int j = 0; j < 3; j++) {
				nn[idx3d(id, i, j, num_devices, nlevel)] = nn[idx3d(id, i - 1, j, num_devices, nlevel)]/2 + 1;
				nn[idx3d(id, i, j, num_devices, nlevel)] = nn[idx3d(id, i - 1, j, num_devices, nlevel)]/2 + 1;
				N[idx2d(id, i, num_devices)] *= nn[idx3d(id, i, j, num_devices, nlevel)];
			}
		}
	}

	//int N = n*n*n;
	double *A;
	double ***A_dev, ***res_dev; // store residual for multigrid
	double ***f_dev;
	double **s_dev, **ss_dev;
	
	CHECK_CUDA(cudaMallocHost(&A,  (n + 1)*(n + 1)*(n + 1)*sizeof(double)));
	CHECK_CUDA(cudaMemset(A, 0.0, (n + 1)*(n + 1)*(n + 1)*sizeof(double)));

	A_dev  = new double**[num_devices];
	f_dev  = new double**[num_devices];
	res_dev  = new double**[num_devices];
	for (int id = 0; id < num_devices; ++id) {
		A_dev[id] = new double*[nlevel];
		res_dev[id] = new double*[nlevel];
		f_dev[id] = new double*[nlevel];
		cudaSetDevice(id);
		for (int i = 0; i < nlevel; i++) {
			CHECK_CUDA(cudaMalloc(&A_dev[id][i],  N[idx2d(id, i, num_devices)]*sizeof(double)));
			CHECK_CUDA(cudaMemset(A_dev[id][i], 0.0, N[idx2d(id, i, num_devices)]*sizeof(double)));
		}
		for (int i = 0; i < nlevel; i++) {
			CHECK_CUDA(cudaMalloc(&res_dev[id][i],  N[idx2d(id, i, num_devices)]*sizeof(double)));
			CHECK_CUDA(cudaMemset(res_dev[id][i], 0.0, N[idx2d(id, i, num_devices)]*sizeof(double)));
		}
		for (int i = 0; i < nlevel; i++) {
			CHECK_CUDA(cudaMalloc(&f_dev[id][i],  N[idx2d(id, i, num_devices)]*sizeof(double)));
			CHECK_CUDA(cudaMemset(f_dev[id][i], 0.0, N[idx2d(id, i, num_devices)]*sizeof(double)));
		}
	}

	ss_dev = new double*[num_devices];
	s_dev  = new double*[num_devices];
	for (int id = 0; id < num_devices; ++id) {
		cudaSetDevice(id);
		CHECK_CUDA(cudaMalloc(ss_dev + id, 1024*sizeof(double)));
		CHECK_CUDA(cudaMalloc(s_dev + id, sizeof(double)));
	}

	// initialize
	//double x, y, z;
	//double t(0.0);
	for (int k = 0; k < n + 1; k++) {
		for (int j = 0; j < n + 1; j++) {
			for (int i = 0; i < n + 1; i++) {
				for (int m = 1; m < 5; m++) {
					A[idx3d(i, j, k, n + 1, n + 1)] += sin(M_PI*m*dx*i)*sin(M_PI*dx*j*(4.0 - m))*sin(2.0*M_PI*dx*k*m)/double(m);
				}
			}
		}
	}
	//cout << A[idx3d(1, 1, 1, n, n)] << endl;

	int offset = 0;
	double *dd = A_dev[0][0];
	for (int id = 0; id < num_devices; ++id) {
		cudaSetDevice(id);
		CHECK_CUDA(cudaMemcpy(A_dev[id][0],  A + offset,
			N[idx2d(id, 0, num_devices)]*sizeof(double), cudaMemcpyHostToDevice));
		//CHECK_CUDA(cudaMemcpy(&A_dev[id][0],  A + offset,  N[idx2d(id, 0, num_devices)]*sizeof(double), cudaMemcpyHostToDevice));
		offset += (nl - 2)*(n + 1)*(n + 1);
	}
	
	float gpu_time = 0.0f;
	//jacobi(n, niter, dx, A_dev, A0_dev, f_dev, ss_dev, s_dev, gpu_time);
	rbgs(num_devices, nlevel, nn, N, niter, dx, A_dev, f_dev, res_dev, ss_dev, s_dev, gpu_time);

	double a(0.0);
	double s0(0.0), s1(0.0);
	//cudaMemcpy(&a, A_dev[1] + idx3d(3, 3, 3, n/2 + 1, n/2 + 1), sizeof(double), cudaMemcpyDeviceToHost);
	CHECK_CUDA(cudaMemcpy(&s0, s_dev[0], sizeof(double), cudaMemcpyDeviceToHost));
	CHECK_CUDA(cudaMemcpy(&s1, s_dev[1], sizeof(double), cudaMemcpyDeviceToHost));

	cout << scientific << "L2 Norm: " << sqrt((s0 + s1)*dx*dx*dx) << endl;
	//cout << scientific << "L2 Norm: " << s << endl;
	//cout << "A(37, 47): " << a << endl;
	cout<<"Time: "<<gpu_time<<" ms"<<endl;


	// Free memory
	//cudaFree(ss_dev);
	//cudaFree(s_dev);
	
	//for (int i = 0; i < nlevel; i++) {
		//cudaFree(A_dev[nlevel]);
		//cudaFree(f_dev[nlevel]);
		//cudaFree(res_dev[nlevel]);
	//}
	//delete A_dev;
	//delete f_dev;
	//delete res_dev;
	//cudaFree(A);
	
	return 0;
}

